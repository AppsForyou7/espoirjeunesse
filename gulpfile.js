"use strict";
var
    gulp             = require('gulp'),
    sass             = require('gulp-sass'),
    cleanCSS         = require('gulp-clean-css'),
    uglify           = require('gulp-uglify'),
    autoprefixer     = require('gulp-autoprefixer'),
    stripCssComments = require('gulp-strip-css-comments'),
    replace          = require('gulp-replace'),
    rev              = require('gulp-rev'),
    concat           = require('gulp-concat'),
    Fontmin          = require('fontmin'),
    preservetime     = require('gulp-preservetime'),
    del              = require('del'),
    ttf2woff2        = require('gulp-ttf2woff2');

// Compile SCSS
gulp.task('css', function() {
    return gulp.src('assets/css/app.scss')
        .pipe(sass({
            includePaths: [
                'node_modules/bootstrap/scss',
                'node_modules/bootstrap/scss/mixins',
                'node_modules/@fortawesome/fontawesome-free/scss',
                'node_modules/datatables.net-bs4/css'
            ]
        }).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(stripCssComments({
            preserve: false
        }))
        .pipe(replace('@charset "UTF-8";', ''))
        .pipe(cleanCSS({compatibility: 'ie8'}))

        .pipe(rev())
        .pipe(gulp.dest('public/assets/css/'))
        .pipe(rev.manifest("rev-manifest.json", {
            base:  '.',
            merge: true,
        }))
        .pipe(gulp.dest('.'))
        ;
});

gulp.task('css_admin', function() {
    return gulp.src('assets_admin/css/app-admin.scss')
        .pipe(sass({
            includePaths: [
                'node_modules/bootstrap/scss',
                'node_modules/bootstrap/scss/mixins',
                'node_modules/@fortawesome/fontawesome-free/scss',
                'node_modules/datatables.net-bs4/css'
            ]
        }).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(stripCssComments({
            preserve: false
        }))
        .pipe(replace('@charset "UTF-8";', ''))
        .pipe(cleanCSS({compatibility: 'ie8'}))

        .pipe(rev())
        .pipe(gulp.dest('public/assets_admin/css/'))
        .pipe(rev.manifest("rev-manifest-admin.json", {
            base:  '.',
            merge: true,
        }))
        .pipe(gulp.dest('.'))
        ;
});

gulp.task('font', function() {
    var fontmin = new Fontmin()
        .src('assets/fonts/**/*.ttf')
        .use(Fontmin.ttf2eot())
        .use(Fontmin.ttf2woff({
            deflate: true,
        }))
        .dest('public/assets/fonts/')
    ;

    fontmin.run(function(err, files) {
        if (err) {
            throw err;
        }

        del.sync(['public/assets/fonts/**/*.ttf']);
        //console.log('Please add fontname.scss in assets/font/css/');
    });

    return gulp.src('assets/fonts/**/*.ttf')
        .pipe(ttf2woff2())
        .pipe(gulp.dest('public/assets/fonts/'))
        .pipe(preservetime())
        ;
});


gulp.task('font_admin', function() {
    var fontmin = new Fontmin()
        .src('assets_admin/fonts/**/*.ttf')
        .use(Fontmin.ttf2eot())
        .use(Fontmin.ttf2woff({
            deflate: true,
        }))
        .dest('public/assets_admin/fonts/')
    ;

    fontmin.run(function(err, files) {
        if (err) {
            throw err;
        }

        del.sync(['public/assets_admin/fonts/**/*.ttf']);
        //console.log('Please add fontname.scss in assets/font/css/');
    });

    return gulp.src('assets_admin/fonts/**/*.ttf')
        .pipe(ttf2woff2())
        .pipe(gulp.dest('public/assets_admin/fonts/'))
        .pipe(preservetime())
        ;
});


gulp.task('js', function() {
    return gulp.src(
        [
            'node_modules/jquery/dist/jquery.js',
            'node_modules/moment/moment.js',
            'node_modules/bootstrap/dist/js/bootstrap.bundle.js',
            'node_modules/bootstrap/dist/js/bootstrap.js',
            'node_modules/jquery.easing/jquery.easing.js',
            'node_modules/datatables.net/js/jquery.dataTables.js',
            'node_modules/datatables.net-bs4/js/dataTables.bootstrap4.js',
            'node_modules/@fortawesome/fontawesome-free/js/all.js',
            'node_modules/@fortawesome/fontawesome-free/js/brands.js',
            'node_modules/@fortawesome/fontawesome-free/js/fontawesome.js',
            'node_modules/@fortawesome/fontawesome-free/js/regular.js',
            'node_modules/@fortawesome/fontawesome-free/js/solid.js',
            'node_modules/@fortawesome/fontawesome-free/js/v4-shims.js',
            'node_modules/chart.js/dist/Chart.js',
            'node_modules/chart.js/dist/Chart.bundle.js',
            'node_modules/datepicker-bootstrap/js/core.js',
            'node_modules/datepicker-bootstrap/js/datepicker.js',
            'node_modules/gijgo/js/gijgo.js',
            'assets/js/app.js'
        ])
        .pipe(concat('public/assets/js/app.js'))
        .pipe(uglify())
        .pipe(rev())
        .pipe(gulp.dest('.'))
        .pipe(rev.manifest("rev-manifest.json", {
            base:  '.',
            merge: true,
        }))
        .pipe(gulp.dest('.'))
        ;
});

gulp.task('js_admin', function() {
    return gulp.src(
        [
            'node_modules/jquery/dist/jquery.js',
            'node_modules/moment/moment.js',
            'node_modules/bootstrap/dist/js/bootstrap.bundle.js',
            'node_modules/bootstrap/dist/js/bootstrap.js',
            'node_modules/jquery.easing/jquery.easing.js',
            'node_modules/datatables.net/js/jquery.dataTables.js',
            'node_modules/datatables.net-bs4/js/dataTables.bootstrap4.js',
            'node_modules/@fortawesome/fontawesome-free/js/all.js',
            'node_modules/@fortawesome/fontawesome-free/js/brands.js',
            'node_modules/@fortawesome/fontawesome-free/js/fontawesome.js',
            'node_modules/@fortawesome/fontawesome-free/js/regular.js',
            'node_modules/@fortawesome/fontawesome-free/js/solid.js',
            'node_modules/@fortawesome/fontawesome-free/js/v4-shims.js',
            'node_modules/chart.js/dist/Chart.js',
            'node_modules/chart.js/dist/Chart.bundle.js',
            'node_modules/datepicker-bootstrap/js/core.js',
            'node_modules/datepicker-bootstrap/js/datepicker.js',
            'node_modules/gijgo/js/gijgo.js',
            'assets_admin/js/pnotify.custom.min.js',
            'assets_admin/js/app-admin.js'
        ])
        .pipe(concat('public/assets_admin/js/app-admin.js'))
        .pipe(uglify())
        .pipe(rev())
        .pipe(gulp.dest('.'))
        .pipe(rev.manifest("rev-manifest-admin.json", {
            base:  '.',
            merge: true,
        }))
        .pipe(gulp.dest('.'))
        ;
});


gulp.task('default', gulp.series('css', 'css_admin', 'font','font_admin', 'js', 'js_admin'));
gulp.task('watch', gulp.series('css', 'css_admin', 'font','font_admin', 'js', 'js_admin', function () {
        gulp.watch(
            [
                'assets/css/**/*.scss',
                'node_modules/bootstrap/scss/**/*.scss'
            ],
            gulp.series('css'));
        gulp.watch(
            [
                'assets_admin/css/**/*.scss',
                'node_modules/bootstrap/scss/**/*.scss'
            ],
            gulp.series('css_admin'));

        gulp.watch(
            [
                'assets/fonts/*',
            ],
            gulp.series('font'));

        gulp.watch(
            [
                'assets_admin/fonts/*',
            ],
            gulp.series('font_admin'));

        gulp.watch(
            [
                'assets/js/**/*.js'
            ],
            gulp.series('js'));

        gulp.watch(
            [
                'assets_admin/js/**/*.js'
            ],
            gulp.series('js_admin'));
    })
);