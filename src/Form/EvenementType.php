<?php

namespace App\Form;

use App\Entity\Evenement;
use App\Entity\StatusEvenement;
use App\Repository\StatusEvenementRepository;
use Doctrine\DBAL\Types\DecimalType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EvenementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class,
                array('attr' => array(
                    'class' => 'form-control', 'placeholder' => ''),
                    'label' => 'Titre','required' => true,
                    'error_bubbling' => true,
                ))
            ->add('imageUpload', FileType::class,
                array('attr' => array(
                    'class' => 'form-control-file'),
                    'label' => 'Choisir une image','required' => false,
                    'error_bubbling' => true,
                ))
            ->add('image', HiddenType::class,
                array('attr' => array(
                    'class' => 'form-control-file'),
                    'label' => 'Choisir une image','required' => true,
                    'error_bubbling' => true,
                ))
            ->add('description', TextareaType::class,
                array('attr' => array(
                    'class' => 'form-control', 'placeholder' => ''),
                    'label' => 'Description','required' => true,
                    'error_bubbling' => true,
                ))
            ->add('dateStart', TextType::class,
                array('attr' => array('class' => 'form-control','readonly'=>'readonly'),
                    'label' => 'Date de début','required' => true,
                    'error_bubbling' => true,
                ))
            ->add('dateEnd', TextType::class,
                array('attr' => array(
                    'class' => 'form-control','readonly'=>'readonly'),
                    'label' => 'Date de fin','required' => true,
                    'error_bubbling' => true,
                ))
            ->add('nbPlace', IntegerType::class,
                array('attr' => array(
                    'class' => 'form-control', 'min'=>1),
                    'label' => 'Nombre de place','required' => true,
                    'error_bubbling' => true,
                ))
            ->add('price', TextType::class,
                array('attr' => array(
                    'class' => 'form-control',"step"=>"0.01"),
                    'label' => 'Prix','required' => true,
                    'error_bubbling' => true,
                ))
            /*->add('statusEvenementId', ChoiceType::class,
                array('attr' => array(
                    'class' => 'form-control', 'placeholder' => ''),
                    'choices'=>['Brouillon'=>1,'En ligne'=>2, 'Terminer'=>3],
                    'label' => 'status','required' => false,
                    'error_bubbling' => true,
                ))*/
            ->add('statusEvenementId', EntityType::class,
                array(
                    'class'=>StatusEvenement::class,
                    'query_builder' => function (StatusEvenementRepository $se) {
                        return $se->createQueryBuilder('u');
                    },
                    'choice_label' => 'status',
                    'attr' => array(
                        'class' => 'form-control'),
                    'label' => 'status','required' => true,
                    'error_bubbling' => true,
                )
            )
            ->add('Sauvegarder', SubmitType::class,
                array('attr' => array('class' => 'btn btn-danger'),
                ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Evenement::class,
        ]);
    }
}
