<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserEditionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lastname', TextType::class,
                array('attr' => array(
                    'class' => 'form-control', 'placeholder' => ''),
                    'label' => 'Nom','required' => true,
                    'error_bubbling' => true,
                ))
            ->add('firstname', TextType::class,
                array('attr' => array(
                    'class' => 'form-control', 'placeholder' => ''),
                    'label' => 'Prénom','required' => true,
                    'error_bubbling' => true,
                ))
            ->add('pseudo', TextType::class,
                array('attr' => array(
                    'class' => 'form-control', 'placeholder' => ''),
                    'label' => 'Pseudo','required' => true,
                    'error_bubbling' => true,
                ))
            ->add('email', TextType::class,
                array('attr' => array(
                    'class' => 'form-control', 'placeholder' => ''),
                    'label' => 'Email','required' => true,
                    'error_bubbling' => true,
                ))
            ->add('address', TextType::class,
                array('attr' => array(
                    'class' => 'form-control', 'placeholder' => ''),
                    'label' => 'Adresse','required' => true,
                    'error_bubbling' => true,
                ))
            ->add('zipCode', TextType::class,
                array('attr' => array(
                    'class' => 'form-control', 'placeholder' => ''),
                    'label' => 'Code Postale','required' => true,
                    'error_bubbling' => true,
                ))
            ->add('city', TextType::class,
                array('attr' => array(
                    'class' => 'form-control', 'placeholder' => ''),
                    'label' => 'Ville','required' => true,
                    'error_bubbling' => true,
                ))
            ->add('plainPassword', RepeatedType::class,
                array(
                'type' => PasswordType::class,
                'invalid_message' => 'Les mots de passe ne correspondent pas',
                'first_options'  => array('attr' => array(
                    'class' => 'form-control'),
                    'label' => 'Mot de passe'),
                'second_options' => array('attr' => array(
                    'class' => 'form-control'),
                    'label' => 'Confirmez votre mot de passe'),
            ))
            ->add('dateOfBirthday', TextType::class,
                array('attr' => array(
                    'class' => 'form-control', 'placeholder' => ''),
                    'label' => 'Date de naissance','required' => true,
                    'error_bubbling' => true,
                ))
            /* ->add('isActive', CheckboxType::class,
                array('attr' => array(
                    'class' => 'form-control', 'placeholder' => ''),
                    'label' => 'Actif ?','required' => false,
                    'error_bubbling' => true,
                ))*/
            ->add('Sauvegarder', SubmitType::class,
                array('attr' => array('class' => 'btn btn-success mt-3'),
                ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
