<?php
/**
 * Created by PhpStorm.
 * User: alexandre
 * Date: 19/09/2018
 * Time: 23:06
 */

namespace App\Controller\Admin;



use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Service\FormService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminUserController extends AbstractController
{
    private $formService;
    private $userRepo;
    private $userService;

    public function __construct
    (
        FormService $formService,
        UserRepository $userRepo,
        UserService $userService
    )
    {
        $this->formService = $formService;
        $this->userRepo = $userRepo;
        $this->userService = $userService;
    }

    /**
     * @Route("/admin/utilisateurs", name="admin_user_home")
     */
    public function adminUserIndex()
    {
        $resultat=$this->userService->getAllUsers();

        return $this->render('admin/user/index.html.twig', [
            'controller_name' => 'AdminController',
            'listUsers' => $resultat['users']
        ]);

    }

    /**
     * @Route("/admin/add/utilisateur", name="admin_user_add")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function adminUserAdd(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserType::class,$user);
        $form->handleRequest($request);
        if($form->isSubmitted()){
            if($form->isValid()){
                $resultat = $this->formService->formEditAddUser($user, 'new');
                if($resultat['etat']=='success'){
                    $this->addFlash('success', $resultat['message']);
                    return $this->redirectToRoute('admin_ev_home');
                }
                elseif($resultat['etat']=='error'){
                    $this->addFlash('danger', $resultat['message']);
                }
            }
            else {
                $this->addFlash('danger', 'Le formulaire n\'est pas valide, veuillez remplir tous les champs');
            }
        }
        return $this->render('admin/user/edit-user.html.twig',
            [
                'form' => $form->createView(),
                'title' => 'Modifier un nouvel utilisateur',
                'path_to_post_form' => $this->generateUrl('admin_user_add'),
                'action'=>'new'
            ]);
    }

    /**
     * @Route("/admin/edit/utilisateur/{user}", name="admin_user_edit")
     * @param Request $request
     * @param $user
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function adminUserEdit(Request $request, User $user)
    {

        if($user){
            $dateBirthday= $user->getDateOfBirthday();
            $dateBirthday = $dateBirthday->format('d/m/Y h:i');
            $user->setDateOfBirthday($dateBirthday);
            $form = $this->createForm(UserType::class,$user);
            $form->handleRequest($request);
            if($form->isSubmitted()){
                if($form->isValid()){
                    $resultat = $this->formService->formEditAddUser($user, 'edit');
                    if($resultat['etat']=='success'){
                        $this->addFlash('success', $resultat['message']);
                        return $this->redirectToRoute('admin_user_home');
                    }
                    elseif($resultat['etat']=='error'){
                        $this->addFlash('danger', $resultat['message']);
                    }
                }
            }
            return $this->render('admin/user/edit-user.html.twig',
                [
                    'form' => $form->createView(),
                    'title' => 'Modifier un nouvel utilisateur',
                    'path_to_post_form' => $this->generateUrl('admin_user_edit',['user'=>$user->getId()]),
                    'action'=>'edit'
                ]);
        }
        else {
            $this->addFlash('danger', 'L\'utilisateur n\' éxiste pas');
            return $this->redirectToRoute('admin_user_home');
        }
    }

    /**
     * @Route("/admin/remove/utilisateur/{user}", name="admin_user_remove")
     * @param $user
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function adminUserRemove($user)
    {
            $resultat = $this->userService->removeUser($user);
            if($resultat['etat']=='success'){
                $this->addFlash('success', $resultat['message']);
                return $this->redirectToRoute('admin_user_home');
            }
            elseif($resultat['etat']=='error'){
                $this->addFlash('danger', $resultat['message']);
                return $this->redirectToRoute('admin_user_home');
            }
    }


}