<?php
/**
 * Created by PhpStorm.
 * User: alexandre
 * Date: 19/09/2018
 * Time: 22:30
 */

namespace App\Controller\Admin;


use App\Entity\Evenement;
use App\Form\EvenementType;
use App\Service\EvenementService;
use App\Service\FormService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminEvenementController extends AbstractController
{
    private $formService;
    private $evenementService;

    public function __construct
    (
        FormService $formService,
        EvenementService $evenementService
    )
    {
        $this->formService = $formService;
        $this->evenementService = $evenementService;
    }

    /**
     * @Route("/admin/evenements", name="admin_ev_home")
     */
    public function adminEvenementIndex()
    {
        $result = $this->evenementService->findAllevenements();
        if($result['code']=='error'){
            $this->addFlash('danger', $result['message']);
        }

        return $this->render('admin/evenement/index.html.twig', [
            'controller_name' => 'AdminController',
            'listEvenements'=>$result['result'],

        ]);
    }

    /**
     * @Route("/admin/evenement/add", name="admin_add_ev")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function adminAddEvenement(Request $request){
        $evenement = new Evenement();
        $form = $this->createForm(EvenementType::class,$evenement);
        $form->handleRequest($request);
        if($form->isSubmitted()){
            if($form->isValid()){
                $resultat = $this->formService->formAddEvenement($evenement);
                if($resultat['etat']=='success'){
                    $this->addFlash('success', $resultat['message']);
                    return $this->redirectToRoute('admin_ev_home');
                }
                elseif($resultat['etat']=='error'){
                    $this->addFlash('danger', $resultat['message']);
                }

            }
            else {
                $this->addFlash('danger', 'Le formulaire n\'est pas valide');
            }
        }
        return $this->render('admin/evenement/new_admin_evenement.html.twig',
            [
                'form' => $form->createView(),
                'title' => 'Ajouter un nouvel évenement',
                'path_to_post_form' => $this->generateUrl('admin_add_ev'),
            ]);
    }

    /**
     * @Route("/admin/evenement/edit/{evenement}", name="admin_edit_ev")
     * @param Request $request
     * @param Evenement $evenement
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function adminEditEvenement(Request $request, Evenement $evenement){
        $dateStart= $evenement->getDateStart();
        $dateEnd= $evenement->getDateEnd();
        $dateStart=$dateStart->format('d/m/Y H:i');
        $dateEnd=$dateEnd->format('d/m/Y H:i');
        $evenement->setDateStart($dateStart);
        $evenement->setDateEnd($dateEnd);
        $form = $this->createForm(EvenementType::class,$evenement);
        $form->handleRequest($request);
        if($form->isSubmitted()){
            if($form->isValid()){
                $resultat = $this->formService->formAddEvenement($evenement);
                if($resultat['etat']=='success'){
                    $this->addFlash('success', $resultat['message']);
                    return $this->redirectToRoute('admin_ev_home');
                }
                elseif($resultat['etat']=='error'){
                    $this->addFlash('danger', $resultat['message']);
                }

            }
            else {
                $this->addFlash('danger', 'Le formulaire n\'est pas valide');
            }
        }
        return $this->render('admin/evenement/new_admin_evenement.html.twig',
            [
                'form' => $form->createView(),
                'title' => 'Ajouter un nouvel évenement',
                'path_to_post_form' => $this->generateUrl('admin_add_ev'),
            ]);
    }

    /**
     * @Route("/admin/evenement/remove/{evenement}", name="admin_remove_ev")
     * @param Request $request
     * @param Evenement $evenement
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function adminRemoveEvenement(Request $request, Evenement $evenement){

        $resultat = $this->formService->removeEvenement($evenement);

        if($resultat['code']=='success'){
            $this->addFlash('success', $resultat['message']);
        }
        elseif($resultat['code']=='error'){
            $this->addFlash('danger', $resultat['message']);
        }
        return $this->redirectToRoute('admin_ev_home');

    }
}