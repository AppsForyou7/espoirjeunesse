<?php
/**
 * Created by PhpStorm.
 * User: alexandre
 * Date: 18/09/2018
 * Time: 12:31
 */

namespace App\Controller\Admin;

use App\Entity\Evenement;
use App\Entity\User;
use App\Repository\EvenementRepository;
use App\Repository\UserRepository;
use App\Service\UserEvenementService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminDefaultController extends AbstractController
{
    private $evenementRepository;
    private $userRepository;
    private $userEvenementService;

    public function __construct
    (
        EvenementRepository $evenementRepository,
        UserRepository $userRepository,
        UserEvenementService $userEvenementService
    )
    {
        $this->evenementRepository = $evenementRepository;
        $this->userRepository = $userRepository;
        $this->userEvenementService = $userEvenementService;
    }

    /**
     * @Route("/admin", name="admin_home")
     */
    public function adminIndex()
    {
        $count['evenements'] = $this->evenementRepository->countAllEvenement();
        $count['users'] = $this->userRepository->countUsersByActive(1);
        $count['register'] = $this->userEvenementService->countRegisterWaiting();
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
            'count' =>$count
        ]);
    }

    /**
     * @Route("/admin/inscrits", name="admin_suscribe_home")
     */
    public function adminSuscribe()
    {
        $result = $this->userEvenementService->findAllSuscribe();
        if($result['code']=='error'){
            $this->addFlash('danger', $result['message']);
        }
        return $this->render('admin/inscrit/index.html.twig', [
            'listSuscribes'=>$result['result']
        ]);
    }

    /**
     * @Route("/admin/inscrit/edit/{evenement}/{user}", name="admin_edit_suscribe")
     * @param Request $request
     * @param Evenement $evenement
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function adminEditSuscribe(Request $request, Evenement $evenement, User $user)
    {
        if($request->request->has('choice')){
            $choice = $request->request->get('choice');
            $result = $this->userEvenementService->editSuscribeByIds($user, $evenement,$choice);
            if($result['code']=='error'){
                $this->addFlash('danger', $result['message']);
            }
            else{
                $this->addFlash('success', $result['message']);
            }

            return $this->redirectToRoute('admin_suscribe_home');
        }
    }
}