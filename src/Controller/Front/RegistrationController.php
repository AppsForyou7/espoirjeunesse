<?php

namespace App\Controller\Front;

use App\Entity\User;
use App\Form\UserPasswordAskType;
use App\Form\UserRegistrationType;
use App\Form\UserResetPasswordType;
use App\Service\FormService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends AbstractController
{
    private $em;
    private $formService;

    public function __construct
    (
        EntityManagerInterface $em,
        FormService $formService
    )
    {
        $this->em = $em;
        $this->formService =$formService;
    }

    /**
     * @Route("/inscription", name="register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function register(Request $request,UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = new User();
        $form=$this->createForm(UserRegistrationType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            if($this->formService->formRegisterUser($user, $passwordEncoder)){
                return $this->redirectToRoute('esj-home');
            }
        }
        return $this->render('Front/registration/register.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/mot-de-passe-oublie", name="esj_ask_password")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function askPasswordAction(Request $request) {
    $user = new User();
    $form = $this->createForm(UserPasswordAskType::class, $user);
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
        if($this->formService->askForNewPassword($user)){
            $this->addFlash("success", "Nous vous avons envoyé un e-mail contenant un lien (valide 12h) pour générer un nouveau mot de passe");
            return $this->redirectToRoute("esj-home");
        }
    }
    return $this->render('Front/Account/Common/ask-password-reset-form.html.twig', [
    "form" => $form->createView(),
    "submit_url" => $this->generateUrl("esj_ask_password"),
    ]);
    }

    /**
     * @Route("/nouveau-motdepasse/{token}", name="esj_reset_password")
     * @param Request $request
     * @param $token
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function resetPasswordAction(Request $request, $token, UserPasswordEncoderInterface $passwordEncoder) {
        $newUser = new User();
        $form = $this->createForm(UserResetPasswordType::class, $newUser);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if($this->formService->updatePassword($token, $newUser, $passwordEncoder)){
                return $this->redirectToRoute('login');
            }
        }
        return $this->render('front/Account/Common/reset-password-form.html.twig', [
            "form" => $form->createView(),
            "submit_url" => $this->generateUrl("esj_reset_password", ["token" => $token]),
        ]);
    }

    /**
     * @Route("/confirmation-compte/{token}", name="esj_user_confirmation_link")
     * @param Request $request
     * @param $token
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function confirmAccountAction(Request $request, $token) {
        $this->formService->activateNewUser($token);
        return $this->redirectToRoute("esj-home");
    }

}
