<?php

namespace App\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

class HelperController extends Controller
{
    /**
     * Taking an array as an input and turning it into a json object
     * @param array
     * @return \Symfony\Component\HttpFoundation\Response
     */
    static function returnResult($output_)
    {
        $encoders = array(new JsonEncoder());
        $normalizers = array(new GetSetMethodNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $response = new \Symfony\Component\HttpFoundation\Response($serializer->serialize($output_, 'json'));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Generating a token
     * @param  int $userId_
     * @param  int $length_
     * @param  int $salt_
     * @return string
     */
    static function generateSecret($userId_,$length_,$salt_){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length_; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString.$salt_.base64_encode($userId_);
    }

    static  function generateThumbnail($file,$sourceAbsolutePath,$sourceRelativePath,$fileName)
    {
        $infosImage = getimagesize($sourceAbsolutePath."/".$fileName);
        $pos = array('x1' =>0,'y1' =>0,'w' =>$infosImage[0],'h'=>$infosImage[1]);
        return HelperController::resizeImage(150, 150, $file, $pos, $sourceAbsolutePath, $fileName, $sourceAbsolutePath."/thumbnails/", $sourceRelativePath."/thumbnails/");
    }

    static  function resizeImage($iWidth, $iHeight,$file,$pos, $sourceAbsolutePath,$fileName,$dstAbsolutePath, $dstPath)
    {
        //$iWidth = $iHeight = 150; // desired image result dimensions
        $iJpgQuality = 90;
        if ($file) {
            if (!is_dir($sourceAbsolutePath)) {
                mkdir($sourceAbsolutePath, 0777, true);
            }
            if (!is_dir($dstAbsolutePath)) {
                mkdir($dstAbsolutePath, 0777, true);
            }
            $aSize = getimagesize($sourceAbsolutePath."/".$fileName); // try to obtain image info
            switch($aSize[2]) {
                case IMAGETYPE_JPEG:
                    $vImg = @imagecreatefromjpeg($sourceAbsolutePath."/".$fileName);
                    break;
                case IMAGETYPE_PNG:
                    $vImg = @imagecreatefrompng($sourceAbsolutePath."/".$fileName);
                    break;
                default:
                    return null;
            }
            if ($vImg != null) {
                $vDstImg = @imagecreatetruecolor($iWidth, $iHeight);
                if ($aSize[2] == IMAGETYPE_PNG) {
                    imagealphablending($vDstImg, false);
                    imagesavealpha($vDstImg, true);
                }
                $checkErrors = imagecopyresampled($vDstImg, $vImg, 0, 0, (int)$pos['x1'], (int)$pos['y1'], $iWidth, $iHeight, (int)$pos['w'], (int)$pos['h']);
                if ($checkErrors == false) {
                    return null;
                }
                if ($aSize[2] == IMAGETYPE_PNG) {
                    $checkErrors = imagepng($vDstImg, $dstAbsolutePath.$fileName, 9);
                }
                else {
                    $checkErrors = imagejpeg($vDstImg, $dstAbsolutePath.$fileName, $iJpgQuality);
                }
                if ($checkErrors == false) {
                    return null;
                }
                return $dstPath.$fileName;
            }
        }
        return null;
    }

    public static function remove_accent($str)
    {
        $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð',
            'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã',
            'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ',
            'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ',
            'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę',
            'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī',
            'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ',
            'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ',
            'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť',
            'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ',
            'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ',
            'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ');

        $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O',
            'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c',
            'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u',
            'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D',
            'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g',
            'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K',
            'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o',
            'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S',
            's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W',
            'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i',
            'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
        return str_replace($a, $b, $str);
    }

    public static function slugifyString($str)
    {
        return mb_strtolower(preg_replace(array('/[^a-zA-Z0-9 \'-]/', '/[ -\']+/', '/^-|-$/'),
            array('', '-', ''), HelperController::remove_accent($str)));
    }

}
