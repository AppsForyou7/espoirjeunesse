<?php
/*
 * Created by PhpStorm.
 * User: alexandre
 * Date: 18/09/2018
 * Time: 12:45
 */

namespace App\Controller\Front;


use App\Entity\Evenement;
use App\Entity\User;
use App\Form\UserRegistrationType;
use App\Repository\EvenementRepository;
use App\Repository\UserRepository;
use App\Repository\UsersEvenementsRepository;
use App\Service\EvenementService;
use App\Service\FormService;
use App\Service\UserEvenementService;
use App\Service\UserService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class DefaultController extends AbstractController
{
    private $evenementRepo;
    private $evenementService;
    private $twigEnvironment;
    private $swiftMailer;
    private $userEvenementService;
    private $usersEvenementsRepository;
    private $userService;
    private $formService;
    private $userRepository;

    public function __construct
    (
        EvenementRepository $evenementRepository,
        EvenementService $evenementService,
        \Swift_Mailer $swiftmailer,
        \Twig_Environment $twigEnvironment,
        UserEvenementService $userEvenementService,
        UsersEvenementsRepository $usersEvenementsRepository,
        UserService $userService,
        FormService $formService,
        UserRepository $userRepository
    )
    {
        $this->evenementRepo = $evenementRepository;
        $this->evenementService = $evenementService;
        $this->swiftMailer = $swiftmailer;
        $this->twigEnvironment = $twigEnvironment;
        $this->userEvenementService = $userEvenementService;
        $this->usersEvenementsRepository = $usersEvenementsRepository;
        $this->userService = $userService;
        $this->formService = $formService;
        $this->userRepository = $userRepository;
    }

    /**
     * @Route("/", name="esj-home")
     */
    public function index()
    {
        //$this->formService->sendTestMail();
        $user = $this->getUser();
        if($user)
            $result= $this->usersEvenementsRepository->findBy(['userId'=>$user]);
        else
            $result = null;
        $evenements = $this->evenementRepo->findActiveEvenements(3);
        return $this->render('front/index.html.twig', [
            'controller_name' => 'DefautlController',
            'evenements'=>$evenements,
            'usersEvenements'=>$result
        ]);
    }


    /**
     * @Route("/tous-les-evenements/{page}", name="esj-all-ev", defaults={"page"=1})
     * @param Request $request
     * @param $page
     * @param PaginatorInterface $paginator
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function allEvenement(Request $request, $page, PaginatorInterface $paginator)
    {
        $query = $this->evenementRepo->findActiveEvenements(0, true);

        $pagination = $paginator->paginate(
            $query,
            $page,
            8
        );
        $pagination->setCustomParameters(['align'=>'center']);
        return $this->render('front/evenement/all-evenements.html.twig', [
            'controller_name' => 'DefautlController',
            'evenements'=>$pagination
        ]);
    }

    /**
     * @Route("/inscription/{user}/{evenement}", name="esj_register_ev")
     * @param Request $request
     * @param User $user
     * @param Evenement $evenement
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function registerEvenement(Request $request, User $user, Evenement $evenement)
    {
        $param=[];
        if($request->headers->has('referer')){
            $refererPath = parse_url($request->headers->get('referer'),PHP_URL_PATH);
            $route = $this->get('router')->match($refererPath)['_route'];
            if($route== "esj-show-evenement"){
                $param=[
                    'slug'=>$evenement->getSlug()
                ];
            }
            $resultat = $this->userEvenementService->registerUserEvenement($user, $evenement);

            if($resultat['etat'] == 'error'){
                $this->addFlash('danger',$resultat['message']);
            }
            else {
                $this->addFlash('success',$resultat['message']);
            }

        }
        else{
            $this->addFlash('error','Une érreur est survenue');
            $route = 'esj-home';
        }
        return $this->redirectToRoute($route, $param);
    }

    /**
     * @Route("/send-mail-contact", name="send-mail-contact")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function sendEmailContact(Request $request)
    {
        if($request->request->has('contact')){
            $contact = $request->request->get('contact');
            if($contact['lastname'] && $contact['firstname'] && $contact['email'] && $contact['message']){
                try {
                    $message = (new \Swift_Message("Prise de contact"))
                        ->setFrom('nassim.mayanegroup@gmail.com','Espoir Jeunesse')
                        ->setTo('tahnint.n@gmail.com')
                        ->setBody(
                            $this->twigEnvironment->render(
                                'Front/mails/contact.html.twig',[
                                    "lastname"=>$contact['lastname'],
                                    "firstname"=>$contact['firstname'],
                                    "email"=>$contact['email'],
                                    "message"=>$contact['message'],
                                    ]
                            ),
                            'text/html'
                        );
                    $this->swiftMailer->send($message);
                    $this->addFlash('success', 'Votre message a bien été envoyé, notre équipe l\'étudiera le plus rapidement possible');
                } catch (\Twig_Error_Loader $e) {
                    $this->addFlash('danger', 'Votre message a bien été envoyé, notre équipe l\'étudiera le plus rapidement possible');
                } catch (\Twig_Error_Runtime $e) {
                    $this->addFlash('danger', 'Votre message a bien été envoyé, notre équipe l\'étudiera le plus rapidement possible');
                } catch (\Twig_Error_Syntax $e) {
                    $this->addFlash('danger', 'Votre message a bien été envoyé, notre équipe l\'étudiera le plus rapidement possible');
                }
            }
            else {
                $this->addFlash('danger', 'Tous les champs \'*\' sont obligatoires');
            }
        }
        else {
            $this->addFlash('danger', 'Le formulaire n\'a pas été envoyé');
        }
        return $this->redirectToRoute('esj-home');
    }


    /**
     * @Route("/profil/{user}", name="esj-profil-user")
     * @param Request $request
     * @param $user
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function profilUser(Request $request, $user, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = $this->userRepository->findOneBy(['slug'=>$user]);
        if($user){
            $resultat = $this->userEvenementService->findAllEvenementsByUser($user,1);
            $user->setDateOfBirthday($user->getDateOfBirthday()->format('d/m/Y'));
            $oldPassword=null;
            if($request->request->has('oldPassword')){
                $oldPassword=$request->request->get('oldPassword');
                if($oldPassword == null || $oldPassword ==""){
                    $user->setPlainPassword('test');
                }
            }

            $form=$this->createForm(UserRegistrationType::class, $user);
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $result=$this->formService->formEditProfile($oldPassword, $user, $passwordEncoder);
                if($result['etat']== 'success'){
                    $this->addFlash('success',$result['message']);
                    return $this->redirectToRoute('esj-profil-user', ['user'=>$user->getId()]);
                }
                $this->addFlash('danger',$result['message']);
            }

            return $this->render('Front/profil/profile.html.twig', [
                'controller_name' => 'DefautlController',
                'listInscription'=>$resultat['list'],
                'form'=>$form->createView()

            ]);
        }
        return $this->redirectToRoute('esj-home');
    }

    /**
     * @Route("/profil/desinscription/{user}/{evenement}", name="esj-unsuscribe-user")
     * @param Request $request
     * @param User $user
     * @param Evenement $evenement
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function unsuscribeEvenement(Request $request, User $user, Evenement $evenement){
        if($request->headers->has('referer')){
            $refererPath = parse_url($request->headers->get('referer'),PHP_URL_PATH);
            $route = $this->get('router')->match($refererPath)['_route'];
            if($route != "esj-profil-user"){
                $this->addFlash('error','Une érreur est survenue');
                return $this->redirectToRoute('esj-home');
            }
            $resultat = $this->userEvenementService->unsuscribeUserEvenement($user, $evenement);
            if($resultat['etat'] == 'error'){
                $this->addFlash('danger',$resultat['message']);
            }
            else {
                $this->addFlash('success',$resultat['message']);
            }
        }
        else{
            $this->addFlash('error','Une érreur est survenue');
            $route = 'esj-home';
        }
        return $this->redirectToRoute($route,['user'=>$user->getSlug()]);
    }

    /**
     * @Route("/evenement/{slug}", name="esj-show-evenement")
     * @param Request $request
     * @param $slug
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function showEvenement(Request $request, $slug){
        $result = $this->evenementService->findArticleBySlug($slug);
        if($result['etat']=='success'){
            return $this->render('Front/evenement/show-evenement.html.twig', [
                "evenement"=>$result["evenement"]
            ]);
        }
        $this->addFlash('danger','Cette article n\'éxiste pas');
        return $this->redirectToRoute('esj-all-ev');
    }

    /**
     * @Route("/qui-somme-nous", name="esj-who-us")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function whoUs()
    {

        return $this->render('Front/others/who-us.html.twig');
    }
}