<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181010115124 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE app_user CHANGE date_of_birthday date_of_birthday DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE evenement CHANGE date_start date_start DATETIME DEFAULT NULL, CHANGE date_end date_end DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE users_evenements ADD user_id INT NOT NULL');
        $this->addSql('ALTER TABLE users_evenements ADD CONSTRAINT FK_36617B94A76ED395 FOREIGN KEY (user_id) REFERENCES app_user (id)');
        $this->addSql('CREATE INDEX IDX_36617B94A76ED395 ON users_evenements (user_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE app_user CHANGE date_of_birthday date_of_birthday DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE evenement CHANGE date_start date_start DATETIME DEFAULT \'NULL\', CHANGE date_end date_end DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE users_evenements DROP FOREIGN KEY FK_36617B94A76ED395');
        $this->addSql('DROP INDEX IDX_36617B94A76ED395 ON users_evenements');
        $this->addSql('ALTER TABLE users_evenements DROP user_id');
    }
}
