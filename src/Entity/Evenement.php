<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EvenementRepository")
 */
class Evenement
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateCreated;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateStart;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateEnd;

    /**
     * @ORM\Column(type="smallint")
     */
    private $nbPlace;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255)
     *
     */
    private $image;

    /**
     *  @Assert\File(
     *     maxSize = "600000",
     *     mimeTypes = {"image/jpeg", "image/png"},
     *     mimeTypesMessage = "Choisir une image du type jpg, jpeg, png"
     * )
     */
    private $imageUpload;

    /**
     * @ORM\Column(type="string")
     */
    private $price;

    /**
    * @ORM\ManyToOne(targetEntity="StatusEvenement", inversedBy="EvenementId")
    * @ORM\JoinColumn(name="statut_evenement_id",nullable=false, referencedColumnName="id")
    */
    private$statusEvenementId;

    /**
     * @ORM\OneToMany(targetEntity="UsersEvenements", mappedBy="evenementId", cascade={"remove"})
     */
    private $evenementUsers;

    public function __construct()
    {
        $this->evenementUsers = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle(string $title)
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription(string $description)
    {
        $this->description = $description;

        return $this;
    }

    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    public function setDateCreated( $dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    public function getDateStart()
    {
        return $this->dateStart;
    }

    public function setDateStart( $dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    public function setDateEnd( $dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    public function getNbPlace()
    {
        return $this->nbPlace;
    }

    public function setNbPlace(int $nbPlace)
    {
        $this->nbPlace = $nbPlace;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getImageUpload()
    {
        return $this->imageUpload;
    }

    /**
     * @param $imageUpload
     */
    public function setImageUpload($imageUpload)
    {
        $this->imageUpload = $imageUpload;
    }

    /**
     * @return mixed
     */
    public function getStatusEvenementId()
    {
        return $this->statusEvenementId;
    }

    /**
     * @param mixed $statusEvenementId
     */
    public function setStatusEvenementId($statusEvenementId)
    {
        $this->statusEvenementId = $statusEvenementId;
    }

    /**
     * @return mixed
     */
    public function getEvenementUsers()
    {
        return $this->evenementUsers;
    }

    /**
     * @param mixed $evenementUsers
     */
    public function setEvenementUsers($evenementUsers): void
    {
        $this->evenementUsers = $evenementUsers;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }


}
