<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsersEvenementsRepository")
 */
class UsersEvenements
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="userEvenements")
     * @ORM\JoinColumn(name="user_id",nullable=false, referencedColumnName="id")
     */
    private $userId;

    /**
     * @ORM\ManyToOne(targetEntity="Evenement", inversedBy="evenementUsers")
     * @ORM\JoinColumn(name="evenement_id",nullable=false, referencedColumnName="id")
     */
    private $evenementId;

    /**
     * @ORM\Column(type="datetime", length=255)
     */
    private $dateRegister;

    /**
     * @ORM\Column(type="smallint")
     */
    private $isRegister;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateRegister()
    {
        return $this->dateRegister;
    }

    public function setDateRegister($dateRegister)
    {
        $this->dateRegister = $dateRegister;

        return $this;
    }

    public function getIsRegister()
    {
        return $this->isRegister;
    }

    public function setIsRegister($isRegister)
    {
        $this->isRegister = $isRegister;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getEvenementId()
    {
        return $this->evenementId;
    }

    /**
     * @param mixed $evenementId
     */
    public function setEvenementId($evenementId)
    {
        $this->evenementId = $evenementId;
    }


}
