<?php
/**
 * Created by PhpStorm.
 * User: alexandre
 * Date: 18/09/2018
 * Time: 15:09
 */

namespace App\Twig;


use Symfony\Component\Asset\Packages;

class RevManifestExtension extends \Twig_Extension
{
    private $asset;

    public function __construct(Packages $asset) {
        $this->asset = $asset;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('assets_versionning', [ $this, 'assetsVersionningFunction' ]),
        );
    }

    public function assetsVersionningFunction($pathOrigin)
    {
        static $jsonData;
        if (!isset($jsonData)) {
            try {
                $jsonData = @json_decode(file_get_contents('./../rev-manifest.json'), true);
            }
            catch (\Exception $e) {
            }
        }
        if($pathOrigin === 'app.css' && @array_key_exists('app.css', $jsonData)){
            return $this->asset->getUrl('/assets/css/'.$jsonData['app.css']);
        }
        if(@array_key_exists($pathOrigin, $jsonData)) {
            $path = explode('/',$jsonData[$pathOrigin]);
            unset($path[0]);
            $path = implode('/',$path);
            return $this->asset->getUrl('/'.$path);
        }
    }

    public function getName()
    {
        return 'assets_versionning_extension';
    }
}