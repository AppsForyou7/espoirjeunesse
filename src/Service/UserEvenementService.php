<?php
/**
 * Created by PhpStorm.
 * User: alexandre
 * Date: 11/10/2018
 * Time: 15:48
 */

namespace App\Service;


use App\Entity\Evenement;
use App\Entity\User;
use App\Entity\UsersEvenements;
use App\Repository\UsersEvenementsRepository;
use Doctrine\ORM\EntityManagerInterface;

class UserEvenementService
{
    private $usersEvenementsRepository;
    private $em;

    public function __construct
    (
        UsersEvenementsRepository $usersEvenementsRepository,
        EntityManagerInterface $em
    )
    {
        $this->usersEvenementsRepository = $usersEvenementsRepository;
        $this->em = $em;
    }

    public function registerUserEvenement(User &$user, Evenement &$evenement){
        $inscrit = $this->usersEvenementsRepository->findOneBy(['userId'=>$user, 'evenementId'=>$evenement]);

        if($inscrit){
            return [
                'etat'=>'error',
                'message'=>'Vous ne pouvez pas vous inscrire plus d\' fois à un même évenement'
            ];
        }

        $userEvenement = new UsersEvenements();
        $userEvenement->setUserId($user);
        $userEvenement->setDateRegister(new \DateTime());
        $userEvenement->setEvenementId($evenement);
        $userEvenement->setIsRegister(1);
        $this->em->persist($userEvenement);

        $oldPlace = $evenement->getNbplace();
        $evenement->setNbPlace($oldPlace -1);
        $this->em->persist($evenement);
        $this->em->flush();
        return [
            'etat'=>'success',
            'message'=>'Votre inscription à bien été pris en compte, elle est en attente du paiement par notre équipe'
        ];
    }

    public function findAllEvenementsByUser($user, $status){
        $list=$this->usersEvenementsRepository->findAllEvenementsByUser($user, $status);
            if($list){
                return [
                    "etat"=>"success",
                    "list"=>$list
                ];
            }
        return [
            "etat"=>"error",
            "list"=>null
        ];
    }

    public function unsuscribeUserEvenement(User $user, Evenement $evenement){
        $inscrit = $this->usersEvenementsRepository->findOneBy(['userId'=>$user, 'evenementId'=>$evenement]);
        if($inscrit){
            if($inscrit->getIsRegister() != 0){
                $oldPlace = $evenement->getNbPlace();
                $evenement->setNbPlace($oldPlace + 1);
                $this->em->persist($evenement);
            }
            $this->em->remove($inscrit);
            $this->em->flush();

            $inscritExist = $this->usersEvenementsRepository->findOneBy(['userId'=>$user, 'evenementId'=>$evenement]);
            if($inscritExist){
                return [
                    'etat'=>'error',
                    'message'=>'La désinscription n\'a pas été pris en compte veuillez ré essayer, si le problème persiste veuillez contacter l\'équipe espoir jeunesse'
                ];
            }
            return [
                'etat'=>'success',
                'message'=>'Votre désinscription à bien été pris en compte'
            ];
        }
        return [
            'etat'=>'error',
            'message'=>'Une érreur est survenue'
        ];
    }

    public function findAllSuscribe(){
        $listSuscribe= $this->usersEvenementsRepository->findAll();
        if(!$listSuscribe){
            return [
                "code"=>"error",
                "message"=> "Aucune Inscription à afficher",
                "result"=>null
            ];
        }
        return [
            "code"=>"success",
            "result"=>$listSuscribe
        ];
    }
    public function findSuscribeByIds($user, $evenement){
        $suscribe= $this->usersEvenementsRepository->findOneBy(['userId'=>$user, 'evenementId'=>$evenement]);
        if(!$suscribe){
            return [
                "code"=>"error",
                "message"=> "Cette inscription n'existe pas",
                "result"=>null
            ];
        }
        return [
            "code"=>"success",
            "result"=>$suscribe
        ];
    }
    public function editSuscribeByIds(User &$user, Evenement &$evenement, $choice){
        $suscribe= $this->usersEvenementsRepository->findOneBy(['userId'=>$user, 'evenementId'=>$evenement]);
        if(!$suscribe){
            return [
                "code"=>"error",
                "message"=> "Cette inscription n'existe pas",
                "result"=>null
            ];
        }
        if($choice == 'Valider'){
            if($suscribe->getIsRegister()==0){
                $oldPlace = $evenement->getNbPlace();
                $evenement->setNbPlace($oldPlace - 1);
            }
            $suscribe->setIsRegister(2);
        }
        elseif ($choice == 'Refuser'){
            $suscribe->setIsRegister(0);
            $oldPlace = $evenement->getNbPlace();
            $evenement->setNbPlace($oldPlace + 1);
        }
        elseif ($choice == 'En attente'){
            if($suscribe->getIsRegister()==0){
                $oldPlace = $evenement->getNbPlace();
                $evenement->setNbPlace($oldPlace - 1);
            }
            $suscribe->setIsRegister(1);
        }
        else{
            return [
                "code"=>"error",
                "message"=>"Une erreure est survenue",
                "result"=>null
            ];
        }
        $this->em->persist($suscribe);
        $this->em->persist($evenement);
        $this->em->flush();
        return [
            "code"=>"success",
            "message"=>"Le status de l'inscription à bien été modifié",
            "result"=>$suscribe
        ];
    }

    public function countRegisterWaiting(){
        return $this->usersEvenementsRepository->count(['isRegister'=>1]);
    }
}