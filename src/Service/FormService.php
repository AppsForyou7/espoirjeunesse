<?php
/**
 * Created by PhpStorm.
 * User: alexandre
 * Date: 22/09/2018
 * Time: 13:45
 */

namespace App\Service;


use App\Controller\Front\HelperController;
use App\Entity\User;
use App\Repository\EvenementRepository;
use App\Repository\StatusEvenementRepository;
use App\Repository\UserRepository;
use App\Repository\UserRoleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class FormService
{
    private $em;
    private $statusEvenementRepository;
    private $evenementRepository;
    private $userRoleRepository;
    private $addFlash;
    private $userRepository;
    private $swiftmailer;
    private $twigEnvironment;

    public function __construct
    (
        EntityManagerInterface $em,
        StatusEvenementRepository $statusEvenementRepository,
        EvenementRepository $evenementRepository,
        UserRoleRepository $userRoleRepository,
        FlashBagInterface $addFlash,
        UserRepository $userRepository,
        \Swift_Mailer $swiftmailer,
        \Twig_Environment $twigEnvironment
    )
    {
        $this->em =$em;
        $this->statusEvenementRepository = $statusEvenementRepository;
        $this->evenementRepository = $evenementRepository;
        $this->userRoleRepository = $userRoleRepository;
        $this->addFlash =$addFlash;
        $this->userRepository = $userRepository;
        $this->swiftmailer = $swiftmailer;
        $this->twigEnvironment = $twigEnvironment;
    }

    public function formAddEvenement($evenement){
        $resultat = $this->uploadFile($evenement);
        if($resultat["etat"]=="error"){
            return [
                'etat'=>'error',
                'message'=> $resultat['message'],
            ];
        }

        $statusEvenement = $this->statusEvenementRepository->find($evenement->getStatusEvenementId());
        if(!$statusEvenement){
            return [
                'etat'=>'error',
                'message'=> 'Le status de l\'evenement est incorrect',
            ];
        }
        $dateStart = new \DateTime($this->convertDateTimeFrOnDateTime($evenement->getDateStart()));
        $dateEnd = new \DateTime($this->convertDateTimeFrOnDateTime($evenement->getDateEnd()));
        if($dateStart->getTimestamp() > $dateEnd->getTimestamp()){
            return [
                'etat'=>'error',
                'message'=> 'La date de fin ne peut se terminer avant le '.$dateStart->format('d/m/Y H:i'),
            ];
        }
        $evenement->setStatusEvenementId($statusEvenement);
        $evenement->setDateCreated(new \DateTime('NOW'));
        $evenement->setDateStart($dateStart);
        $evenement->setDateEnd($dateEnd);
        $evenement->setSlug(HelperController::slugifyString($evenement->getTitle())."-".$evenement->getDateCreated()->getTimestamp());
        $this->em->persist($evenement);
        $this->em->flush();
        if($evenement != null){
            return [
                'etat'=>'success',
                'message'=> 'L\'article a bien été enregistré'
            ];
        }
        else {
            return [
                'etat'=>'error',
                'message'=> 'Le formulaire n\'est pas valide ou n\'a pas été envoyé',
            ];
        }

    }

    public function formEditEvenement($evenement){
        $resultat = $this->uploadFile($evenement, "edit");
        if($resultat["etat"]=="error"){
            return [
                'etat'=>'error',
                'message'=> $resultat['message'],
            ];
        }

        $statusEvenement = $this->statusEvenementRepository->find($evenement->getStatusEvenementId());
        if(!$statusEvenement){
            return [
                'etat'=>'error',
                'message'=> 'Le status de l\'evenement est incorrect',
            ];
        }
        $dateStart = $this->convertDateTimeFrOnDateTime($evenement->getDateStart());
        $dateEnd = $this->convertDateTimeFrOnDateTime($evenement->getDateEnd());

        $evenement->setStatusEvenementId($statusEvenement);
        $evenement->setDateStart(new \DateTime($dateStart));
        $evenement->setDateEnd(new \DateTime($dateEnd));
        $evenement->setSlug(HelperController::slugifyString($evenement->getTitle())."-".$evenement->getDateCreated()->getTimestamp());
        $this->em->persist($evenement);
        $this->em->flush();
        if($evenement != null){
            return [
                'etat'=>'success',
                'message'=> 'L\'evenement a bien été modifié'
            ];
        }
        else {
            return [
                'etat'=>'error',
                'message'=> 'Le formulaire n\'est pas valide ou n\'a pas été envoyé',
            ];
        }

    }

    private function uploadFile($entity){
        $file = $entity->getImageUpload();
        if(!$file){
            if($entity->getImage() == '' && $entity->getImage() == null){
                return [
                    "etat"=>"error",
                    "message"=>"Veuillez uploader une image",
                ];
            }
        }
        else {
            $filename = $this->generateUniqueFileName().'.'.$file->guessExtension();
            if(!is_dir("uploads")) {
                mkdir('uploads', 0777, true);
            }
            if(!is_dir("uploads/evenements")){
                mkdir('uploads/evenements', 0777, true);
            }

            try{
                $file->move(
                    'uploads/evenements',
                    $filename
                );
            } catch (FileException $e){
                return [
                    "etat"=>"error",
                    "message"=>"Une érreur est survenue avec l'upload de fichier"
                ];
            }
            if($entity->getImage() !='' && $entity->getImage() != null){
                if(!unlink('uploads/evenements/'.$entity->getImage())){
                    return [
                        "etat"=>"error",
                        'message'=>'Une erreur est survenue'
                    ];
                }
            }
            $entity->setImage($filename);
        }
        return [
            "etat"=>"success"
        ];
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }

    public function formEditAddUser($user, $action){
        $dateBrirthday = $this->convertDateTimeFrOnDateTime($user->getDateOfBirthday());
        $user->setDateOfBirthday(new \DateTime($dateBrirthday));
        $this->em->persist($user);
        $this->em->flush();
        if($user != null){
            if($action == "new")
                $message="L'utilisateur à bien été ajouté";
            else
                $message="L'utilisateur à bien été modifié";
            $etat="success";
        }
        else {
            $etat="error";
            $message = "Le formulaire n'est pas valide ou n'a pas été envoyé";
        }
        return [
            'etat'=>$etat,
            'message'=> $message,
        ];
    }

    public function formEditProfile($oldPassowrd, $user, UserPasswordEncoderInterface $passwordEncoder){

        $dateBrirthday = $this->convertDateFrOnDateTime($user->getDateOfBirthday());
        $user->setDateOfBirthday(new \DateTime($dateBrirthday));
        $message="";
        if($oldPassowrd!= null || $oldPassowrd != ""){
            $oldPassowrdEncode=$passwordEncoder->isPasswordValid($user, $oldPassowrd);
            if($oldPassowrdEncode){
                $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
                $user->setPassword($password);
            }
            else {
                return [
                    'etat'=>"error",
                    'message'=> "Votre mot de passe acutel est éronnée",
                ];
            }
        }

        $user->setDateUpdate(new \DateTime());
        $this->em->persist($user);
        $this->em->flush();


        if($user != null){

            $message.="Votre profile à bien été modifié";
            $etat="success";
        }
        else {
            $etat="error";
            $message="Une erreur est survenu lors de votre modification de profile";
        }
        return [
            'etat'=>$etat,
            'message'=> $message,
        ];
    }

    public function removeEvenement($evenement){
        $verif=$evenement->getId();
        if(!unlink('uploads/evenements/'.$evenement->getImage())){
            return [
                "etat"=>"error",
                'message'=>'Une erreur est survenue'
            ];
        }
       $this->em->remove($evenement);
       $this->em->flush();
       $existE = $this->evenementRepository->find($verif);

       if(!$existE){
           return [
               "code"=>"success",
               "message"=>"L'évenement a bien été supprimé"
           ];
       }
        return [
            "code"=>"error",
            "message"=>"Une erreur est survenue, l'évenement n'a pas été supprimé"
        ];
    }

    public function convertDateTimeFrOnDateTime($date){
        $tabDate = explode(' ', $date);
        $ds= explode('/', $tabDate[0]);
        $newDate = $ds[2].'-'.$ds[1].'-'.$ds[0].' '.$tabDate[1];
        return $newDate;
    }
    public function convertDateFrOnDateTime($date){
        $ds= explode('/', $date);
        $newDate = $ds[2].'-'.$ds[1].'-'.$ds[0].' 00:00';
        return $newDate;
    }


    public function askForNewPassword($user)
    {
        $foundUser=$this->userRepository->findOneBy(['email'=>$user->getEmail()]);
        if(!$foundUser){
            $this->addFlash->add('danger','Aucun compte ne correspond à cette email');
            return false;
        }

        $foundUser->setPasswordToken($foundUser->generateRegistrationToken());
        $foundUser->setDatePasswordToken(new \DateTime());
        $this->em->persist($foundUser);
        $this->em->flush();
        $this->sendPasswordResetLinkMail($foundUser);
        return true;
    }

    private function sendPasswordResetLinkMail(User $foundUser) {
        //send a mail with the confirmation link
        try {
            $message = (new \Swift_Message("Mot de passe oublié sur le site d'Espoir Jeunesse"))
                ->setFrom('nassim.mayanegroup@gmail.com','Espoir Jeunesse')
                ->setTo($foundUser->getEmail())
                ->setBody(
                    $this->twigEnvironment->render(
                        'Front/mails/Account/forgot-password-regeneration-mail.html.twig',
                        ["user" => $foundUser]
                    ),
                    'text/html'
                )
            ;

            $this->swiftmailer->send($message);
        } catch (\Twig_Error_Loader $e) {
        } catch (\Twig_Error_Runtime $e) {
        } catch (\Twig_Error_Syntax $e) {
        }
    }

    public function updatePassword($token, $user, UserPasswordEncoderInterface $passwordEncoder)
    {
        $messages = [];
            $foundUser = $this->userRepository->findOneBy(['passwordToken'=>$token]);
            if(!$foundUser){
                $this->addFlash->add('danger', 'Lien de demande de mot de passe invalide');
                    return false;
            }
            if (strlen($user->getPlainPassword()) < 5) {
                $messages[] = "Votre mot de passe doit faire au moins 6 caractères";
            }
            if (strpos($user->getPlainPassword(), $foundUser->getPseudo()) !== false) {
                $messages[] = "Votre mot de passe ne peut contenir votre pseudo";
            }


        if (!$this->checkLinkValidity($foundUser)) {
            $messages[] = "Ce lien est expiré";
        }

        if (count($messages) > 0) {
            foreach ($messages as $message) {
                $this->addFlash->add('danger', $message);
            }
            return false;
        }

        $password = $passwordEncoder->encodePassword($foundUser, $user->getPlainPassword());
        $foundUser->setPassword($password);
        $foundUser->setDatePasswordToken(null);
        $foundUser->setPasswordToken(null);
        $this->em->persist($foundUser);
        $this->em->flush();
        $this->addFlash->add('success','Votre mot de passe a bien été enregistré.');
        return true;
    }

    private function checkLinkValidity(User &$user)
    {
        $date_request = $user->getDatePasswordToken();
        $diff = $date_request->diff(new \Datetime());
        if ($diff->y == 0 && $diff->m == 0 && $diff->d == 0 && $diff->h < 12) {
            return true;
        }
        return false;
    }


    public function formRegisterUser(User $user, UserPasswordEncoderInterface $passwordEncoder){

        $messages = [];

        $this->checkIsNotEmptyAndNull($user, $messages);

        $this->checkEmail($user, $messages);

        $this->checkPseudo($user, $messages);

        $this->checkPassword($user, $messages);


        if (count($messages) > 0) {
            foreach ($messages as $message) {
                $this->addFlash->add('danger', $message);
            }
            return false;
        }

        $dateBrirthday = $this->convertDateFrOnDateTime($user->getDateOfBirthday());
        $user->setDateOfBirthday(new \DateTime($dateBrirthday));
        $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
        $user->setPassword($password);
        $userRole = $this->userRoleRepository->find(1);
        $user->setRole($userRole);
        $user->setRegistrationToken($user->generateRegistrationToken());
        $user->setSlug($user->getPseudo().'-'.date_timestamp_get(new \DateTime()));
        $user->setDateCreated(new \DateTime());
        $user->setDateUpdate(new \DateTime());
        $user->setIsActive(0);
        $this->em->persist($user);
        $this->em->flush();
        $this->sendConfirmationMail($user);
        $this->addFlash->add('success', 'Validé ! Merci de votre inscription sur Espoir Jeunesse ! Nous venons de vous envoyer un email de confirmation pour activer votre compte. Si vous ne le voyez pas, il est peut-être dans vos spams.');
        return true;
    }

    private function checkEmail(User &$user, array &$messages) {
        //check if email is correctly formatted
        if (!filter_var($user->getEmail(), FILTER_VALIDATE_EMAIL)) {
            $messages[] = "Veuillez saisir un e-mail valide";
        }
        //check if email already exist in database
        $exist = $this->userRepository->findOneBy(["email" => $user->getEmail()]);
        if ($exist != null) {
            $messages[] = "Cette adresse e-mail est déjà enregistrée";
        }
    }

    private function checkPseudo(User &$user, array &$messages) {
        //check if email is correctly formatted
        if (strlen($user->getPseudo()) < 5 || strlen($user->getPseudo()) > 30) {
            $messages[] = "Votre pseudo doit faire entre 6 et 30 caractères";
        }

        if (!preg_match("/^[A-Za-z0-9_]+$/",$user->getPseudo())) {
            $messages[] = "Votre pseudo ne peut contenir que des lettres, chiffres ou underscore";
        }

        if (!preg_match('/[a-zA-Z]/', $user->getPseudo())) {
            $messages[] = "Votre pseudo doit contenir au moins une lettre";
        }

        //check if email already exist in database
        $exist = $this->userRepository->findOneBy(["pseudo" => $user->getPseudo()]);
        if ($exist != null) {
            $messages[] = "Ce pseudo est déjà enregistrée";
        }
    }

    private function checkPassword(User &$user, array &$messages) {
        if (strlen($user->getPlainPassword()) < 5) {
            $messages[] = "Votre mot de passe doit faire au moins 6 caractères";
        }
        if (strpos($user->getPlainPassword(), $user->getPseudo()) !== false) {
            $messages[] = "Votre mot de passe ne peut contenir votre pseudo";
        }
    }

    private function checkIsAValidDate(User &$user, array &$messages){
        $dateBrirthday = $this->convertDateFrOnDateTime($user->getDateOfBirthday());
        if((bool)strtotime($user->getDateOfBirthday())){
            $this->checkBirthday($user, $messages);
        }
        else{
            $messages[] = "Vous devez avoir une date valide";
        }
    }

    private function checkBirthday(User &$user, array &$messages) {
        $now = new \DateTime();
        $birthday = $user->getDateOfBirthday();

        $interval = $birthday->diff($now);
        if ($interval->y > 17) {
            $messages[] = "Vous devez avoir moins de 18 ans";
        }
    }

    private function checkIsNotEmptyAndNull(User &$user, array &$messages){
        if ($user->getLastname() == null || $user->getLastname() == "") {
            $messages[] = "Le nom ne peut etre vide";
        }
        if ($user->getFirstname() == null || $user->getFirstname() == "") {
            $messages[] = "Le prénom ne peut etre vide";
        }
        if ($user->getPseudo() == null || $user->getPseudo() == "") {
            $messages[] = "Le pseudo ne peut etre vide";
        }
        if ($user->getEmail() == null || $user->getEmail() == "") {
            $messages[] = "L'email ne peut etre vide";
        }
        if ($user->getDateOfBirthday() == null || $user->getDateOfBirthday() == "") {
            $messages[] = "La date de naissance ne peut etre vide";
        }
        if ($user->getAddress() == null || $user->getAddress() == "") {
            $messages[] = "L'adresse ne peut etre vide";
        }
        if ($user->getZipCode() == null || $user->getZipCode() == "") {
            $messages[] = "Le code postale ne peut etre vide";
        }
        if ($user->getCity() == null || $user->getCity() == "") {
            $messages[] = "La ville ne peut etre vide";
        }
        if ($user->getPlainPassword() == null || $user->getPlainPassword() == "") {
            $messages[] = "Le mot de passe ne peut etre vide";
        }

    }

    private function sendConfirmationMail(User $user) {
        //send a mail with the confirmation link
        try {
            $message = (new \Swift_Message("Bienvenue sur le site d'Espoir Jeunesse"))
                ->setFrom('nassim.mayanegroup@gmail.com')
                ->setTo($user->getEmail())
                ->setBody(
                    $this->twigEnvironment->render(
                        'Front/mails/Account/registration-confirmation-mail.html.twig',
                        ["user" => $user]
                    ),
                    'text/html'
                )
            ;
            $this->swiftmailer->send($message);
        } catch (\Twig_Error_Loader $e) {
        } catch (\Twig_Error_Runtime $e) {
        } catch (\Twig_Error_Syntax $e) {
        }
    }


    public function activateNewUser(string $token)
    {
        $user = $this->userRepository->findOneBy(["isActive" => 0, "registrationToken" => $token]);
        if ($user == null) {
            $this->addFlash->add("danger", "Lien de confirmation invalide");
            return false;
        }
        else {
            $this->addFlash->add("success", "Validé ! Votre compte a été activé. Vous pouvez vous connecter.");
        }
        $user->setIsActive(1);
        $user->setRegistrationToken(null);
        $this->em->persist($user);
        $this->em->flush();
        return true;
    }

    public function sendTestMail(){
        $userA = $this->userRepository->find(7);

        try {
            $message = (new \Swift_Message("Bienvenue sur le site d'Espoir Jeunesse"))
                ->setFrom('nassim.mayanegroup@gmail.com')
                ->setTo($userA->getEmail())
                ->setBody(
                    $this->twigEnvironment->render(
                        'Front/mails/Account/registration-confirmation-mail.html.twig',
                        ["user" => $userA]
                    ),
                    'text/html'
                )
            ;
            $this->swiftmailer->send($message);
        } catch (\Twig_Error_Loader $e) {
        } catch (\Twig_Error_Runtime $e) {
        } catch (\Twig_Error_Syntax $e) {
        }
    }

}