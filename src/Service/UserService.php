<?php
/**
 * Created by PhpStorm.
 * User: alexandre
 * Date: 22/09/2018
 * Time: 13:45
 */

namespace App\Service;


use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

class UserService
{
    private $em;
    private $userRepo;

    public function __construct
    (
        EntityManagerInterface $em,
        UserRepository $userRepo
    )
    {
        $this->em =$em;
        $this->userRepo =$userRepo;
    }

    public function getAllUsers(){
        $users=$this->userRepo->findBy(['role'=> 1]);
        if($users){
            return [
                'etat'=>'error',
                'users'=> $users
            ];
        }
        return [
            'etat'=>'error',
            'users'=>null

        ];

    }

    public function removeUser($user){
            $user->setActive(0);
            $this->em->persist($user);
            $this->em->flush();
            if($user->getActive != 0){
                return [
                    'etat'=>'error',
                    'message'=> 'Une erreur est survenue l\'utilisateur n\'a pas été désactivé'
                ];
            }
            else {
                return [
                    'etat'=>'success',
                    'message'=> 'L\'utilisateur à bien été désactivé',
                ];
            }
    }
}