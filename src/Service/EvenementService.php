<?php
/**
 * Created by PhpStorm.
 * User: alexandre
 * Date: 07/10/2018
 * Time: 22:04
 */

namespace App\Service;


use App\Repository\EvenementRepository;
use Doctrine\ORM\EntityManagerInterface;

class EvenementService
{
    private $em;
    private $evenementRepository;

    public function __construct
    (
        EntityManagerInterface $em,
        EvenementRepository $evenementRepository
    )
    {
        $this->em =$em;
        $this->evenementRepository = $evenementRepository;
    }

    public function findAllevenements(){
        $evenements= $this->evenementRepository->findAll();
        if(!$evenements){
            return [
                "code"=>"error",
                "message"=> "Aucun Evenement à afficher",
                "result"=>null
            ];
        }
        return [
            "code"=>"success",
            "result"=>$evenements
        ];
    }

    public function findArticleBySlug($slug){
        $result = $this->evenementRepository->findOneBy(['slug'=>$slug,'statusEvenementId'=>2]);

        if($result){
            return [
                'etat'=>'success',
                'evenement'=>$result
            ];
        }
        return [
            'etat'=>'error'
        ];
    }
}