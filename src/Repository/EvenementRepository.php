<?php

namespace App\Repository;

use App\Entity\Evenement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Evenement|null find($id, $lockMode = null, $lockVersion = null)
 * @method Evenement|null findOneBy(array $criteria, array $orderBy = null)
 * @method Evenement[]    findAll()
 * @method Evenement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvenementRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Evenement::class);
    }

    public function countAllEvenement(){
        return $this->createQueryBuilder('e')
            ->select('count(e)')
            ->getQuery()
            ->getSingleScalarResult()
            ;

    }

    public function findActiveEvenements($limit = 0, $onlyQuery = false){
        $qb= $this->createQueryBuilder('e')
            ->where('e.statusEvenementId = :status')
            ->setParameter('status', 2)
            ->andWhere('e.dateStart > :now')
            ->setParameter('now',  new \DateTime('NOW'));
        if($limit && is_integer($limit) && $limit >0){
            $qb->setMaxResults($limit);
        }
        if($onlyQuery){
            return $qb->getQuery();
        }
        return $qb->getQuery()
            ->getResult();
    }

//    /**
//     * @return Evenement[] Returns an array of Evenement objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Evenement
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
