<?php

namespace App\Repository;

use App\Entity\UsersEvenements;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UsersEvenements|null find($id, $lockMode = null, $lockVersion = null)
 * @method UsersEvenements|null findOneBy(array $criteria, array $orderBy = null)
 * @method UsersEvenements[]    findAll()
 * @method UsersEvenements[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsersEvenementsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UsersEvenements::class);
    }

    public function findAllEvenementsByUser($user, $status){
        $qb = $this->createQueryBuilder('ue')
            ->where('ue.userId = :id')
            ->setParameter('id', $user->getId());
        return $qb->getQuery()->getResult();

    }

//    /**
//     * @return UsersEvenements[] Returns an array of UsersEvenements objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UsersEvenements
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
