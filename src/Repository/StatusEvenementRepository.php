<?php

namespace App\Repository;

use App\Entity\StatusEvenement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method StatusEvenement|null find($id, $lockMode = null, $lockVersion = null)
 * @method StatusEvenement|null findOneBy(array $criteria, array $orderBy = null)
 * @method StatusEvenement[]    findAll()
 * @method StatusEvenement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatusEvenementRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, StatusEvenement::class);
    }

//    /**
//     * @return StatusEvenement[] Returns an array of StatusEvenement objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StatusEvenement
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
